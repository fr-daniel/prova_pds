package br.ufc.questao_2;

public class App {

	public static void main(String[] args) {
		Padaria padaria = new Padaria();

		Cliente cliente = new Cliente("Daniel");
		Cliente cliente1 = new Cliente("José");
		
		padaria.addObservadores(cliente, cliente1);
		
		padaria.adicionarFornadaPao(new FornadaDePao(10));
		padaria.adicionarFornadaPao(new FornadaDePao(40));
		
	}

}
