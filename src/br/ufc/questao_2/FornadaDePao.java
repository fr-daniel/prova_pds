package br.ufc.questao_2;

public class FornadaDePao {

	private int qtdPaes;
	
	public FornadaDePao(int qtdPaes) {
		this.qtdPaes = qtdPaes;
	}
	
	public int getQtdPaes() {
		return qtdPaes;
	}
	
}
