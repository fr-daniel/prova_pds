package br.ufc.questao_2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Padaria {
	
	private List<FornadaDePao> fornadasDePao;
	private List<Observador> clientes; 
	
	public Padaria() {
		this.fornadasDePao = new ArrayList<FornadaDePao>();
		this.clientes = new ArrayList<Observador>();
	}
	
	
	public void addObservador(Observador observador) {
		this.clientes.add(observador);
	}
	
	public void removeObservador(Observador observador) {
		this.clientes.remove(observador);
	}
	
	public void addObservadores(Observador ...observadores) {
		this.clientes.addAll(Arrays.asList(observadores));
	}
	
	public void adicionarFornadaPao(FornadaDePao fornadaDePao) {
		this.fornadasDePao.add(fornadaDePao);
		notifique(fornadaDePao);
	}
	
	private void notifique(FornadaDePao fornadaDePao) {
		clientes.forEach(cliente -> cliente.atualize(fornadaDePao));
	}
}
