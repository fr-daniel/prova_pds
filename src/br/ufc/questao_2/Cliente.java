package br.ufc.questao_2;

public class Cliente implements Observador {
	
	private String nome;
	
	public Cliente(String nome) {
		this.nome = nome;
	}

	@Override
	public void atualize(FornadaDePao fornadaDePao) {
		System.out.println(this.nome + ": Oba! O padeiro fez uma nova fornada com " + fornadaDePao.getQtdPaes() + " pães.");
	}

}
