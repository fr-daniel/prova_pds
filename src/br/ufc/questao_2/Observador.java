package br.ufc.questao_2;

public interface Observador {

	public void atualize(FornadaDePao fornadaDePao);
	
}
