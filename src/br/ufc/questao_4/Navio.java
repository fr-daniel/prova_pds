package br.ufc.questao_4;

public class Navio {

	private Arma arma;
	
	public Navio(Arma arma) {
		this.arma = arma;
	}
	
	public void atirar() {
		this.arma.atirar();
	}

	public void setArma(Arma arma) {
		this.arma = arma;
	}
	
}
