package br.ufc.questao_4;

public class App {

	public static void main(String[] args) {
		
		Navio navio = new Navio(new BalaBazuca());
		
		navio.atirar();
		navio.atirar();
		
		navio.setArma(new BalaCanhao());
		
		navio.atirar();
		
		navio.setArma(new BalaBazuca());
		navio.atirar();
	}

}
