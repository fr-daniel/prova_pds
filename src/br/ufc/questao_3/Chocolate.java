package br.ufc.questao_3;

public class Chocolate extends SorveteDecorator {

	public Chocolate(Sorvete sorvete) {
		super(sorvete);
		setDescricao("chocolate");
	}

}
