package br.ufc.questao_3;

public abstract class SorveteDecorator extends Sorvete {

	private Sorvete sorvete;
	
	public SorveteDecorator(Sorvete sorvete) {
		this.sorvete = sorvete;
	}
	
	@Override
	public String getDescricao() {
		return sorvete.getDescricao() + ", " + super.getDescricao();
	}
	
}
