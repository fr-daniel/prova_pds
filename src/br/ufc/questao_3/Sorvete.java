package br.ufc.questao_3;

public abstract class Sorvete {

	private String descricao;
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}

}
